﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApiMongo.Models;

public class Book
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    [BsonElement("Name")]
    public string BookName { get; set; } = null!;

    public decimal Price { get; set; }

    public string Category { get; set; } = null!;

    public string Author { get; set; } = null!;

    public string Password { get; set; } = BCrypt.Net.BCrypt.HashPassword("Pa$$w0rd");
    //string passwordHash = BCrypt.Net.BCrypt.HashPassword("Pa$$w0rd");
    public DateTime DateTime { get; set; }
}